FROM python:3.10
WORKDIR /app
ENV PYTHONPATH /app/src

RUN apt-get update -q && apt-get install -y build-essential automake pkg-config libtool libffi-dev libgmp-dev libsecp256k1-dev postgresql-client git make curl && \
    rm -rf /var/cache/apk/*

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
