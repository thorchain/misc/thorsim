from utils.common import HttpClient


class THORClient(HttpClient):
    """
    A client implementation to thorchain API
    """

    def __init__(self, api_url):
        super().__init__(api_url)

    def get_block_height(self):
        """
        Get the current block height of thorchain
        """
        data = self.fetch("/thorchain/lastblock")
        if data is None:
            return 0
        return int(data[0]["thorchain"])

    def get_vault_address(self, chain):
        data = self.fetch("/thorchain/inbound_addresses")
        for d in data:
            if chain == d["chain"]:
                return d["address"]
        return "address not found"

    def get_vault_pubkey(self):
        data = self.fetch("/thorchain/inbound_addresses")
        return data[0]["pub_key"]

    def get_vault_data(self, height=None):
        url = "/thorchain/network"
        if height:
            url = f"/thorchain/network?height={height}"
        return self.fetch(url)

    def get_asgard_vaults(self):
        return self.fetch("/thorchain/vaults/asgard")

    def get_yggdrasil_vaults(self):
        return self.fetch("/thorchain/vaults/yggdrasil")

    def get_module_balance(self, mod):
        result = self.fetch("/thorchain/balance/module/{mod}")
        for coin in result["coins"]:
            if coin["denom"] == "rune":
                return int(coin["amount"])

    def get_supply(self):
        result = self.fetch("/cosmos/bank/v1beta1/supply")
        return result["supply"]

    def get_pools(self):
        return self.fetch("/thorchain/pools")

    def get_pool(self, asset):
        for p in self.get_pools():
            if p["asset"] == asset:
                return p
        return None
