from utils.common import Asset
from chains.chain import GenericChain


class Litecoin(GenericChain):
    """
    A local simple implementation of litecoin chain
    """

    name = "Litecoin"
    chain = "LTC"
    coin = Asset("LTC.LTC")
