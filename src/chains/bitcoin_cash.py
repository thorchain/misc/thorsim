from utils.common import Asset
from chains.chain import GenericChain


class BitcoinCash(GenericChain):
    """
    A local simple implementation of bitcoin chain
    """

    name = "Bitcoin Cash"
    chain = "BCH"
    coin = Asset("BCH.BCH")
