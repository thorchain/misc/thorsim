from utils.common import Asset, Coin
from chains.chain import GenericChain


class Ethereum(GenericChain):
    """
    A local simple implementation of Ethereum chain
    """

    name = "Ethereum"
    gas_per_byte = 68
    chain = "ETH"
    coin = Asset("ETH.ETH")

    @classmethod
    def _calculate_gas(cls, pool, txn):
        """
        Calculate gas according to RUNE thorchain fee
        """
        gas = 39540
        if txn.gas is not None and txn.gas[0].asset.is_eth():
            gas = txn.gas[0].amount
        if txn.memo == "WITHDRAW:ETH.ETH:1000":
            gas = 39839
        elif txn.memo.startswith("SWAP:ETH.ETH:"):
            gas = 39827
        elif txn.memo.startswith(
            "SWAP:ETH.TKN-0X40BCD4DB8889A8BF0B1391D0C819DCD9627F9D0A"
        ):
            gas = 53215
        elif (
            txn.memo
            == "WITHDRAW:ETH.TKN-0X40BCD4DB8889A8BF0B1391D0C819DCD9627F9D0A:1000"
        ):
            gas = 53227
        elif txn.memo == "WITHDRAW:ETH.TKN-0X40BCD4DB8889A8BF0B1391D0C819DCD9627F9D0A":
            gas = 44822
        elif txn.memo == "WITHDRAW:ETH.ETH":
            gas = 39851
        return Coin(cls.coin, gas * 3)
