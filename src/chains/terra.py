from utils.common import Asset
from chains.chain import GenericChain


class Terra(GenericChain):
    """
    A local simple implementation of Terra chain
    """

    name = "Terra"
    chain = "TERRA"
    coin = Asset("TERRA.LUNA")
