from utils.common import Asset
from chains.chain import GenericChain


class Dogecoin(GenericChain):
    """
    A local simple implementation of dogecoin chain
    """

    name = "Dogecoin"
    chain = "DOGE"
    coin = Asset("DOGE.DOGE")
