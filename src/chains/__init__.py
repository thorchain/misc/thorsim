from chains.terra import Terra
from chains.bitcoin import Bitcoin
from chains.bitcoin_cash import BitcoinCash
from chains.binance import Binance
from chains.dogecoin import Dogecoin
from chains.litecoin import Litecoin
from chains.ethereum import Ethereum
from chains.thorchain import Thorchain

CHAINS = [
    Thorchain,
    Terra,
    Bitcoin,
    BitcoinCash,
    Binance,
    Dogecoin,
    Litecoin,
    Ethereum,
]
