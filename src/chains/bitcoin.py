from utils.common import Asset
from chains.chain import GenericChain


class Bitcoin(GenericChain):
    """
    A local simple implementation of bitcoin chain
    """

    name = "Bitcoin"
    chain = "BTC"
    coin = Asset("BTC.BTC")
