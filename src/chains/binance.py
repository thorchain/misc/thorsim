from utils.common import Asset
from chains.chain import GenericChain


class Binance(GenericChain):
    """
    A local simple implementation of binance chain
    """

    name = "Binance"
    chain = "BNB"
    coin = Asset("BNB.BNB")
