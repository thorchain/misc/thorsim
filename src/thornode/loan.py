import logging
from pprint import pprint

from utils.common import (
    Jsonable,
)


class Loan(Jsonable):
    def __init__(self, address):
        self.address = address
        self.rune_deposit_value = 0
        self.asset_deposit_value = 0
        self.last_add_height = 0
        self.debt = 0

    def open(self, debt, rune_deposit_value, asset_deposit_value, last_add_height):
        self.debt += debt
        self.rune_deposit_value += rune_deposit_value
        self.asset_deposit_value += asset_deposit_value
        self.last_add_height = last_add_height
