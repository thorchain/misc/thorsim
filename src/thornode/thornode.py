import os
import time
import logging
import statistics
from pprint import pprint

from copy import copy, deepcopy

from utils.common import (
    Coin,
    Asset,
    Jsonable,
    get_rune_asset,
    get_share,
    print_amount,
    Transaction,
)

from thornode.swap_queue import SwapQueue
from thornode.pool import Pool

from client.thorchain import THORClient

from chains import CHAINS
from chains.terra import Terra
from chains.ethereum import Ethereum

RUNE = get_rune_asset()
SNAPSHOT = os.getenv("SNAPSHOT", "False").lower() in ("true", "1", "t")


class THORNode(Jsonable):
    """
    A complete implementation of the thorchain logic/behavior
    """

    rune_fee = 2000000
    synth_multiplier = 1

    def __init__(self):
        self.height = 0
        self.pools = []
        self.reserve = 0
        self.liquidity = {}
        self.total_bonded = 0
        self.bond_reward = 0
        self.network_fees = {}
        self.swap_queue = SwapQueue()
        self.supply = {}
        self.maxAnchorSlip = 1500 # mimir for max anchor slip
        self.usd_vault = 0
        self.history = []

    def begin_block(self):
        self.height += 1
        self.spawn_derived_pools()

    def end_block(self):
        start = time.time()
        swaps = self.swap_queue.fetch_swaps(self.pools)
        emit = None
        for swap in swaps:
            emit = self.swap(swap.txn)

        # track pool slip per block for derived assets
        for pool in self.pools:
            pool.slips.append(pool.slip)
            pool.slip = 0

        self.snapshot()
        return emit

    def snapshot(self):
        # track state for each block
        # c = copy(self)
        # c.history = []
        # self.history.append(c)
        pass

    def add_swap_queue(self, txn):
        self.swap_queue.add(txn)

    def get_supply(self, denom):
        if denom not in self.supply:
            return 0
        return self.supply[denom]

    def mint(self, asset, amt):
        if asset not in self.supply:
            self.supply[asset] = 0
        logging.debug(f"Minting: {asset} {print_amount(amt)}")
        supply = self.get_supply(asset)
        self.supply[asset] = supply + int(amt)

    def burn(self, asset, amt):
        logging.debug(f"Burning: {asset} {print_amount(amt)}")
        supply = self.get_supply(asset)
        self.supply[asset] = supply - int(amt)

    def get_pool(self, asset):
        """
        Fetch a specific pool by asset
        """
        if Asset(asset).is_synth:
            asset = Asset(asset).get_layer1_asset()
        for pool in self.pools:
            if pool.asset == asset:
                return pool

        return Pool(asset)

    def set_pool(self, pool):
        """
        Set a pool
        """
        for i, p in enumerate(self.pools):
            if p.asset == pool.asset:
                self.pools[i] = pool
                return

        self.pools.append(pool)

    def _total_liquidity(self):
        """
        Total up the liquidity fees from all pools
        """
        total = 0
        for value in self.liquidity.values():
            total += value
        return total

    def get_gas_asset(self, chain):
        for c in CHAINS:
            if chain == c:
                return c.coin
        return None

    def get_gas(self, chain, tx):
        if chain == "ETH":
            return Ethereum._calculate_gas(None, tx)
        return self.get_max_gas(chain)

    def get_max_gas(self, chain):
        if chain == "THOR":
            return Coin(RUNE, self.rune_fee)
        rune_fee = self.get_rune_fee(chain)
        gas_asset = self.get_gas_asset(chain)
        pool = self.get_pool(gas_asset)
        if chain == "BTC":
            amount = int(self.btc_tx_rate * 3 / 2) * self.btc_estimate_size
        if chain == "BCH":
            amount = int(self.bch_tx_rate * 3 / 2) * self.bch_estimate_size
        if chain == "LTC":
            amount = int(self.ltc_tx_rate * 3 / 2) * self.ltc_estimate_size
        if chain == "DOGE":
            amount = int(self.doge_tx_rate * 3 / 2) * self.doge_estimate_size
        if chain == "TERRA":
            amount = int(self.terra_tx_rate * 3 / 2) * self.terra_estimate_size
            amount = int(amount / 100) * 100  # round TERRA to 6 digits max
        if chain == "BNB":
            amount = pool.get_rune_in_asset(int(rune_fee / 3))
        return Coin(gas_asset, amount)

    def get_rune_fee(self, chain):
        if chain not in self.network_fees:
            return self.rune_fee
        chain_fee = self.network_fees[chain]
        if chain_fee == 0:
            return self.rune_fee
        gas_asset = self.get_gas_asset(chain)
        pool = self.get_pool(gas_asset)
        if pool.asset_balance == 0 or pool.rune_balance == 0:
            return self.rune_fee
        chain_fee = chain_fee * 3
        if chain == "TERRA":
            chain_fee = int(chain_fee / 100) * 100
        return pool.get_asset_in_rune(chain_fee)

    def get_asset_fee(self, chain):
        if chain in self.network_fees:
            asset_fee = self.network_fees[chain] * 3
            if chain == "TERRA":
                asset_fee = int(asset_fee / 100) * 100
            return asset_fee
        gas_asset = self.get_gas_asset(chain)
        pool = self.get_pool(gas_asset)
        return pool.get_rune_in_asset(self.rune_fee)

    def refund(self, txn, code, reason):
        logging.debug(f"Refund: {txn} {code} {reason}")

    def swap(self, tx):
        """
        Does a swap (or double swap)
        MEMO: SWAP:<asset(req)>:<address(op)>:<target_trade(op)>
        """
        logging.debug(f"Swap: {tx}")
        # parse memo
        parts = tx.memo.split(":")
        if len(parts) < 2:
            if tx.memo == "":
                return self.refund(tx, 105, "memo can't be empty")
            return self.refund(tx, 105, f"invalid tx type: {tx.memo}")

        address = tx.from_address
        # check address to send to from memo
        if len(parts) > 2 and parts[2] != "":
            address = parts[2]

        # get trade target, if exists
        target_trade = 0
        if len(parts) > 3:
            target_trade = int(parts[3] or "0")

        asset = Asset(parts[1])

        # check that we have one coin
        if len(tx.coins) != 1:
            reason = "not expecting multiple coins in a swap: unknown request"
            return self.refund(tx, 105, reason)

        source = tx.coins[0].asset
        target = asset

        # refund if we're trying to swap with the coin we given ie
        # swapping bnb with bnb
        if source == target and source.is_synth == target.is_synth:
            reason = "swap Source and Target cannot be the same.: unknown request"
            return self.refund(tx, 105, reason)

        if (
            ("thor" in tx.to_address or "SYNTH" in tx.to_address)
            and target.get_chain() != "THOR"
        ):
            reason = (
                "swap destination address is not the same chain as "
                "the target asset: unknown request"
            )
            return self.refund(tx, 105, reason)
        pool = self.get_pool(target)
        if target.is_rune():
            pool = self.get_pool(source)

        if pool.is_zero():
            return self.refund(tx, 108, f"{asset} pool doesn't exist")

        thor_usd = self.get_pool("THOR.USD")
        pools = []
        in_tx = tx

        # check if we have enough to cover the fee
        rune_fee = self.get_rune_fee(target.get_chain())
        in_coin = in_tx.coins[0]
        if in_coin.is_rune() and in_coin.amount <= rune_fee:
            return self.refund(tx, 108, "fail swap, not enough fee")

        # check if its a double swap
        if not source.is_rune() and not target.is_rune():
            pool = self.get_pool(source)
            if pool.is_zero():
                return self.refund(tx, 108, "fail swap, invalid balance")

            emit, liquidity_fee, liquidity_fee_in_rune, swap_slip, pool = self.swap_one(
                tx.coins[0], RUNE
            )

            # check if we have enough to cover the fee
            if emit.is_rune() and emit.amount <= rune_fee:
                return self.refund(tx, 108, "fail swap, not enough fee")

            if pool.asset.is_derived_asset():
                usd_value = thor_usd.get_rune_in_asset(liquidity_fee_in_rune)
                self.usd_vault += usd_value
                self.mint("usd", usd_value)
                self.burn(tx.coins[0].asset, tx.coins[0].amount)
                if tx.coins[0].asset.is_rune():
                    self.mint(target, emit.amount)
                else:
                    self.mint('rune', emit.amount)
            else:
                if str(pool.asset) not in self.liquidity:
                    self.liquidity[str(pool.asset)] = 0
                self.liquidity[str(pool.asset)] += liquidity_fee_in_rune
                pool.slip += swap_slip

            # here we copy the tx to break references cause
            # the tx is split in 2 events and gas is handled only once
            in_tx = deepcopy(tx)

            self.set_pool(pool)
            in_tx.coins[0] = emit
            source = RUNE

        # set asset to non-rune asset
        asset = source
        if asset.is_rune():
            asset = target

        # check if we have enough to cover the fee
        rune_fee = self.get_rune_fee(target.get_chain())
        in_coin = in_tx.coins[0]
        if in_coin.is_rune() and in_coin.amount <= rune_fee:
            return self.refund(tx, 108, "fail swap, not enough fee")

        pool = self.get_pool(asset)

        if target.is_synth and pool.is_zero():
            return self.refund(tx, 108, f"{asset} pool doesn't exist")

        if pool.is_zero():
            return self.refund(tx, 108, "fail swap, invalid balance")

        emit, liquidity_fee, liquidity_fee_in_rune, swap_slip, pool = self.swap_one(
            in_tx.coins[0], target
        )
        pools.append(pool)

        # check if we have enough to cover the fee
        if emit.is_rune() and emit.amount <= rune_fee:
            return self.refund(
                tx,
                108,
                f"output RUNE ({emit.amount}) is not enough to pay transaction fee",
            )

        # check emit is non-zero and is not less than the target trade
        if emit.is_zero() or (emit.amount < target_trade):
            reason = f"emit asset {emit.amount} less than price limit {target_trade}"
            return self.refund(tx, 108, reason)

        if pool.asset.is_derived_asset():
            usd_value = thor_usd.get_rune_in_asset(liquidity_fee_in_rune)
            self.usd_vault += usd_value
            self.mint("usd", usd_value)
            self.burn(in_tx.coins[0].asset, in_tx.coins[0].amount)
            if in_tx.coins[0].asset.is_rune():
                self.mint(target, emit.amount)
            else:
                self.mint('rune', emit.amount)
        else:
            if str(pool.asset) not in self.liquidity:
                self.liquidity[str(pool.asset)] = 0
            self.liquidity[str(pool.asset)] += liquidity_fee_in_rune
            pool.slip += swap_slip

        # save pools
        for pool in pools:
            self.set_pool(pool)

        return emit

    def swap_one(self, coin, asset):
        """
        Does a single swap returning amount of coins emitted and new pool

        :param Coin coin: coin sent to swap
        :param Asset asset: target asset
        :returns: list of events
            - emit (int) - number of coins to be emitted for the swap
            - liquidity_fee (int) - liquidity fee
            - liquidity_fee_in_rune (int) - liquidity fee in rune
            - swap_slip (int) - trade slip
            - pool (Pool) - pool with new values

        """
        if not coin.is_rune():
            asset = coin.asset

        pool = self.get_pool(asset)
        if coin.is_rune():
            X = pool.rune_balance
            Y = pool.asset_balance
        else:
            X = pool.asset_balance
            Y = pool.rune_balance

        if asset.is_synth:
            X = X * self.synth_multiplier
            Y = Y * self.synth_multiplier

        x = coin.amount
        emit = self._calc_asset_emission(X, x, Y)
        # decimals to 6 if TERRA chain
        if asset.chain == Terra.chain:
            emit = int(emit / 100) * 100

        # calculate the liquidity fee (in rune)
        liquidity_fee = self._calc_liquidity_fee(X, x, Y)
        # decimals to 6 if TERRA chain
        # if asset.chain == Terra.chain:
        #     liquidity_fee = int(liquidity_fee / 100) * 100

        liquidity_fee_in_rune = liquidity_fee
        if coin.is_rune():
            liquidity_fee_in_rune = pool.get_asset_in_rune(liquidity_fee)

        # calculate trade slip
        swap_slip = self._calc_swap_slip(X, x)

        # if we emit zero, return immediately
        if emit == 0:
            return Coin(asset, emit), 0, 0, 0, pool

        new_pool = deepcopy(pool)  # copy of pool
        if coin.is_rune():
            new_pool.add(x, 0)
            if asset.is_synth:
                new_pool.synth_balance += emit
            else:
                new_pool.sub(0, emit)
            emit = Coin(asset, emit)
        else:
            if asset.is_synth:
                new_pool.synth_balance -= x
            else:
                new_pool.add(0, x)
            new_pool.sub(emit, 0)
            emit = Coin(RUNE, emit)
        self.set_pool(new_pool)

        return emit, liquidity_fee, liquidity_fee_in_rune, swap_slip, new_pool

    def _calc_liquidity_fee(self, X, x, Y):
        """
        Calculate the liquidity fee from a trade
        ( x^2 *  Y ) / ( x + X )^2

        :param int X: first balance
        :param int x: asset amount
        :param int Y: second balance
        :returns: (int) liquidity fee

        """
        return int(float((x ** 2) * Y) / float((x + X) ** 2))

    def _calc_swap_slip(self, X, x):
        """
        Calculate the trade slip from a trade
        expressed in basis points (10,000)
        x / (X + x)

        :param int X: first balance
        :param int x: asset amount
        :returns: (int) trade slip

        """
        swap_slip = 10000 * x / (X + x)
        return int(round(swap_slip))

    def _calc_asset_emission(self, X, x, Y):
        """
        Calculates the amount of coins to be emitted in a swap
        ( x * X * Y ) / ( x + X )^2

        :param int X: first balance
        :param int x: asset amount
        :param int Y: second balance
        :returns: (int) asset emission

        """
        return int((x * X * Y) / (x + X) ** 2)

    def get_usd_anchors(self):
        return [
            Asset("BNB.BUSD-BD1"),
            Asset("ETH.USDC-0XA0B86991C6218B36C1D19D4A2E9EB0CE3606EB48"),
            Asset("ETH.USDT-0XDAC17F958D2EE523A2206206994597C13D831EC7"),
            Asset("ETH.DAI-0X6B175474E89094C44DA98B954EEDEAC495271D0F"),
            Asset("TERRA.UST"),
        ]

    def spawn_derived_pools(self):
        anchors = {
            Asset("THOR.USD"): self.get_usd_anchors()
        }
        for c in CHAINS:
            if c.chain == "THOR":
                continue
            derived = c.coin
            derived.chain = "THOR"
            anchors[derived] = [c.coin]

        for d, anc in anchors.items():
            total_rune = 0
            slips = []
            price = []
            for anchor in anc:
                anchor_pool = self.get_pool(anchor)

                if anchor_pool.status != "Available":
                    continue
                
                if anchor_pool.rune_balance <= 0 or anchor_pool.asset_balance <= 0:
                    continue

                total_rune += anchor_pool.rune_balance
                slips.append(sum(anchor_pool.get_slip()))
                price.append(anchor_pool.rune_balance / anchor_pool.asset_balance)

            if total_rune == 0:
                continue

            slippage = statistics.median(slips)
            price = statistics.median(price)

            rune_depth = 0
            min_rune_depth = total_rune / 1000 # rune depth cannot fall below 1%
            if slippage <= self.maxAnchorSlip:
                rune_depth = get_share(self.maxAnchorSlip-slippage, 10000, total_rune)
            if rune_depth < min_rune_depth:
                rune_depth = min_rune_depth

            asset_depth = int(rune_depth / price)
            
            d = Asset(d).get_derived_asset()
            p = Pool(d, rune_depth, asset_depth, "Available")
            self.set_pool(p)

    def populate(self, url="http://thornode.thorchain.info"):
        """
        Populates the THORChain state with data from a live THORChain network,
        defaults to mainnet
        """
        self.client = THORClient(url)
        pools = self.client.get_pools()
        for p in pools:
            pool = Pool(
                p["asset"], int(p["balance_rune"]), int(p["balance_asset"]), p["status"]
            )
            pool.lp_units = int(p['LP_units'])
            pool.synth_balance = int(p['synth_supply'])
            self.pools.append(pool)
        # TODO: add LPs

        self.reserve = self.client.get_module_balance("reserve")
        self.asgard = self.client.get_module_balance("asgard")
        self.total_bonded = self.client.get_module_balance("bond")
        self.height = self.client.get_block_height()

        supply = self.client.get_supply()
        for s in supply:
            self.supply[s["denom"]] = int(s["amount"])

        self.spawn_derived_pools()
        self.snapshot()

    def generate_arb_txn(self, asset, target, step=10000):
        pool = self.get_pool(asset)

        if pool.rune_balance == 0:
            raise "invalid balance to arbitrage"
        if pool.asset_balance == 0:
            raise "invalid balance to arbitrage"

        ratio = pool.rune_balance / pool.asset_balance

        if ratio == target:
            # pool is already balanced
            return None

        amt = 0
        x = 0
        if ratio > target:
            while amt == 0:
                x += step
                emit = self._calc_asset_emission(pool.rune_balance, x, pool.rune_balance)
                new_ratio = (pool.rune_balance - emit) / (pool.asset_balance + x)
                if target <= new_ratio:
                    continue
                else:
                    amt = x
                    break
            swap = Transaction(
                pool.asset.chain,
                f"{pool.asset.chain.lower()}1_arber",
                f"{RUNE.chain.lower()}1yyyyy",
                Coin(asset, amt),
                f"~:{RUNE}:{RUNE.chain.lower()}1_arber",
            )
        else:
            while amt == 0:
                x += step
                emit = self._calc_asset_emission(pool.rune_balance, x, pool.asset_balance)
                new_ratio = (pool.rune_balance + x) / (pool.asset_balance - emit)
                if target >= new_ratio:
                    continue
                else:
                    amt = x
                    break
            return Transaction(
                pool.asset.chain,
                f"{RUNE.chain.lower()}1_arber",
                f"{pool.asset.chain.lower()}1yyyyy",
                Coin(RUNE, amt),
                f"~:{pool.asset}:{pool.asset.chain.lower()}1_arber",
            )

    def pprint(self):
        p = self.__dict__
        p['history'] = "redacted"
        pprint(p)
