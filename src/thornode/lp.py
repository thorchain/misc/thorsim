import logging

from utils.common import (
    Jsonable,
)


class LiquidityProvider(Jsonable):
    def __init__(self, address, units=0):
        self.address = address
        self.units = 0
        self.pending_rune = 0
        self.pending_asset = 0
        self.pending_tx = None
        self.rune_deposit_value = 0
        self.asset_deposit_value = 0
        self.rune_address = None
        self.asset_address = None

    def add(self, units):
        """
        Add liquidity provider units
        """
        self.units += units

    def sub(self, units):
        """
        Subtract liquidity provider units
        """
        self.units -= units
        if self.units < 0:
            logging.error(f"Overdrawn liquidity provider: {self}")
            raise Exception("insufficient liquidity provider units")

    def is_zero(self):
        return self.units <= 0

    def __repr__(self):
        return "<Liquidity Provider %s Units: %d>" % (self.address, self.units)

    def __str__(self):
        addr = self.address
        if addr is None:
            addr = self.rune_address
        if addr is None:
            addr = self.asset_address
        return "Liquidity Provider %s Units: %d" % (addr, self.units)
