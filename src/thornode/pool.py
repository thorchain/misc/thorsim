import logging
from pprint import pprint 

from utils.common import (
    Jsonable,
    Asset,
    get_share,
)

from thornode.lp import LiquidityProvider
from thornode.loan import Loan

from chains.terra import Terra


class Pool(Jsonable):
    def __init__(self, asset, rune_amt=0, asset_amt=0, status="Available"):
        if isinstance(asset, str):
            self.asset = Asset(asset)
        else:
            self.asset = asset
        self.rune_balance = rune_amt
        self.asset_balance = asset_amt
        self.synth_balance = 0
        self.lp_units = 0
        self.liquidity_providers = []
        self.status = status
        self.fees_collected = 0
        self.slip = 0  # track pool slip this block
        self.slips = []  # track historical pool slip
        self.loans = []

    def get_slip(self, blocks=300):
        """
        get the slips for the last x blocks
        """
        if len(self.slips) > blocks:
            return self.slips[-blocks:]
        return self.slips

    def get_asset_in_rune(self, val):
        """
        Get an equal amount of given value in rune
        """
        if self.is_zero():
            return 0

        return get_share(self.rune_balance, self.asset_balance, val)

    def get_rune_reimbursement_for_asset_withdrawal(self, val):
        """
        Get equivalent amount of rune for a given amount of asset withdrawn
        from the pool, taking slip into account. When this amount is added
        to the pool, the constant product of depths rule is preserved.
        """
        if self.is_zero():
            return 0

        return get_share(self.rune_balance, self.asset_balance - val, val)

    def get_rune_in_asset(self, val):
        """
        Get an equal amount of given value in asset
        """
        if self.is_zero():
            return 0

        amount = get_share(self.asset_balance, self.rune_balance, val)
        if self.asset.chain == Terra.chain:
            amount = int(amount / 100) * 100
        return amount

    def get_rune_disbursement_for_asset_add(self, val):
        """
        Get the equivalent amount of rune for a given amount of asset added to
        the pool, taking slip into account. When this amount is withdrawn from
        the pool, the constant product of depths rule is preserved.
        """
        if self.is_zero():
            return 0

        return get_share(self.rune_balance, self.asset_balance + val, val)

    def get_asset_fee(self):
        """
        Calculates how much asset we need to pay for the 1 rune transaction fee
        """
        if self.is_zero():
            return 0

        return self.get_rune_in_asset(2000000)

    def sub(self, rune_amt, asset_amt):
        """
        Subtracts from pool
        """
        self.rune_balance -= rune_amt
        self.asset_balance -= asset_amt

        if self.asset_balance < 0 or self.rune_balance < 0:
            logging.error(f"Overdrawn pool: {self}")
            raise Exception("insufficient funds")

    def add(self, rune_amt, asset_amt):
        """
        Add to pool
        """
        self.rune_balance += rune_amt
        self.asset_balance += asset_amt

    def is_zero(self):
        """
        Check if pool has zero balance
        """
        return self.rune_balance == 0 and self.asset_balance == 0

    def synth_units(self):
        """
        Calculate dynamic synth units
        (L*S)/(2*A-S)
        L = LP units
        S = synth balance
        A = asset balance
        """
        if self.asset_balance == 0:
            return 0
        numerator = self.lp_units * self.synth_balance
        denominator = 2 * self.asset_balance - self.synth_balance
        if denominator == 0:
            denominator = 1
        return int(numerator / denominator)

    def pool_units(self):
        """
        Calculate total pool units
        (L+S)
        L = LP units
        S = synth balance
        """
        return self.synth_units() + self.lp_units

    def get_liquidity_provider(self, address):
        """
        Fetch a specific liquidity provider by address
        """
        for lp in self.liquidity_providers:
            if lp.address == address:
                return lp

        return LiquidityProvider(address)

    def set_liquidity_provider(self, lp):
        """
        Set a liquidity provider
        """
        for i, s in enumerate(self.liquidity_providers):
            if s.address == lp.address:
                self.liquidity_providers[i] = lp
                return

        self.liquidity_providers.append(lp)

    def add_liquidity(
        self, rune_address, asset_address, rune_amt, asset_amt, txid
    ):
        """
        add liquidity rune/asset for an address
        """
        fetch_address = asset_address
        if rune_address is not None:
            fetch_address = rune_address
        lp = self.get_liquidity_provider(fetch_address)
        if lp.rune_address is None:
            lp.rune_address = rune_address
        if lp.asset_address is None:
            lp.asset_address = asset_address

        asset_amt += lp.pending_asset
        rune_amt += lp.pending_rune

        # handle cross chain liquidity provision
        if asset_amt == 0 and asset_address is not None:
            lp.pending_rune += rune_amt
            lp.pending_tx = txid
            self.set_liquidity_provider(lp)
            print("foo")
            return 0, 0, 0, None
        if rune_amt == 0 and rune_address is not None:
            lp.pending_asset += asset_amt
            lp.pending_tx = txid
            self.set_liquidity_provider(lp)
            print("bar")
            return 0, 0, 0, None

        lp.pending_rune = 0
        lp.pending_asset = 0
        units = self._calc_liquidity_units(
            self.rune_balance,
            self.asset_balance,
            rune_amt,
            asset_amt,
        )

        self.add(rune_amt, asset_amt)
        self.lp_units += units
        lp.units += units
        lp.rune_deposit_value += get_share(units, self.lp_units, self.rune_balance)
        lp.asset_deposit_value += get_share(units, self.lp_units, self.asset_balance)
        self.set_liquidity_provider(lp)
        return units, rune_amt, asset_amt, lp.pending_tx

    def withdraw(self, address, withdraw_basis_points):
        """
        Withdraw from an address with given withdraw basis points
        """
        if withdraw_basis_points > 10000 or withdraw_basis_points < 0:
            raise Exception("withdraw basis points should be between 0 - 10,000")

        lp = self.get_liquidity_provider(address)
        units, rune_amt, asset_amt = self._calc_withdraw_units(
            lp.units, withdraw_basis_points
        )
        # decimals to 6 if TERRA chain
        if self.asset.chain == Terra.chain:
            asset_amt = int(asset_amt / 100) * 100
        lp.units -= units
        lp.rune_deposit_value -= get_share(units, self.lp_units, self.rune_balance)
        lp.asset_deposit_value -= get_share(units, self.lp_units, self.asset_balance)
        self.set_liquidity_provider(lp)
        self.lp_units -= units
        self.sub(rune_amt, asset_amt)
        return units, rune_amt, asset_amt

    def _calc_liquidity_units(self, R, A, r, a):
        """
        Calculate liquidity provider units
        slipAdjustment = (1 - ABS((R a - r A)/((r + R) (a + A))))
        units = ((P (a R + A r))/(2 A R))*slidAdjustment
        R = pool rune balance after
        A = pool asset balance after
        r = provided rune
        a = provided asset
        """
        P = self.pool_units()
        R = float(R)
        A = float(A)
        r = float(r)
        a = float(a)
        if R == 0.0 or A == 0.0 or P == 0:
            return int(r)
        slipAdjustment = 1 - abs((R * a - r * A) / ((r + R) * (a + A)))
        units = (P * (a * R + A * r)) / (2 * A * R)
        return int(units * slipAdjustment)

    def _calc_withdraw_units(self, lp_units, withdraw_basis_points):
        """
        Calculate amount of rune/asset to withdraw
        Returns liquidity provider units, rune amount, asset amount
        """
        units_to_claim = get_share(withdraw_basis_points, 10000, lp_units)
        withdraw_rune = get_share(units_to_claim, self.pool_units(), self.rune_balance)
        withdraw_asset = get_share(
            units_to_claim, self.pool_units(), self.asset_balance
        )
        units_after = lp_units - units_to_claim
        if units_after < 0:
            logging.error(f"Overdrawn liquidity provider units: {self}")
            raise Exception("Overdrawn liquidity provider units")
        return units_to_claim, withdraw_rune, withdraw_asset

    def get_loan(self, address):
        """
        Fetch a specific loan by address
        """
        for loan in self.loans:
            if loan.address == address:
                return loan

        return Loan(address)

    def set_loan(self, loan):
        """
        Set a loan
        """
        for i, l in enumerate(self.loans):
            if l.address == loan.address:
                self.loans[i] = loan
                return

        self.loans.append(loan)

    def open_loan(self, height, address, units, minCR=100, maxCR=200):
        rune_deposit_value = get_share(units, self.pool_units(), self.rune_balance)
        asset_deposit_value = get_share(units, self.pool_units(), self.asset_balance)

        lp = self.get_liquidity_provider(address)
        loan = self.get_loan(address)

        if lp.units < units:
            raise "lp does not have enough LP units"

        # calculate CR
        cr = self._calc_cr(units, minCR, maxCR)

        runeUSD = 10.0 # TODO: get price from THOR.USD anchors
        collateral_value_in_rune = self.get_asset_in_rune(asset_deposit_value) + rune_deposit_value
        collateral_value_in_usd = collateral_value_in_rune * runeUSD / 1e8
        print(collateral_value_in_usd)
        debt = collateral_value_in_usd / (cr / 100.0)

        loan.open(debt, rune_deposit_value, asset_deposit_value, height)
        lp.units -= units
        lp.rune_deposit_value -= rune_deposit_value
        lp.asset_deposit_value -= asset_deposit_value

        self.set_loan(loan)
        self.set_liquidity_provider(lp)

    def _calc_cr(self, cu, minCR=100, maxCR=500):
        # ((cu / pu) * (maxCR-minCR)) + minCR
        for loan in self.loans:
            cu += loan.units
        print(f"({cu} / {self.pool_units()}) * (maxCR-minCR) + minCR")
        return ((cu / self.pool_units()) * (maxCR-minCR)) + minCR

    def pprint(self):
        pprint(self.__dict__)

    def __repr__(self):
        return "<Pool %s Rune: %d | Asset: %d>" % (
            self.asset,
            self.rune_balance,
            self.asset_balance,
        )

    def __str__(self):
        return (
            "Pool %s Rune: %d | Asset: %d | Units: %s | Synth Units: %s | Synth: %s"
            % (
                self.asset,
                self.rune_balance,
                self.asset_balance,
                self.lp_units,
                self.synth_units(),
                self.synth_balance,
            )
        )
