from utils.common import (
    Coin,
    Asset,
    Jsonable,
    get_rune_asset,
)

RUNE = get_rune_asset()


class SwapQueue(Jsonable):
    def __init__(self, min_swaps=10, max_swaps=100):
        self.min_swaps = min_swaps
        self.max_swaps = max_swaps
        self.queue = []

    def len(self):
        return len(self.queue)

    def add(self, txn):
        self.queue.append(SwapQueueItem(txn))

    def sort(self, pools):
        for item in self.queue:
            item.calc_slip_and_fee(pools)

        for i, item in enumerate(sorted(self.queue, key=lambda x: x.fee, reverse=True)):
            item.score = i

        for i, item in enumerate(
            sorted(self.queue, key=lambda x: x.slip, reverse=True)
        ):
            item.score += i

        self.queue = sorted(self.queue, key=lambda x: x.score, reverse=False)

    def todo(self):
        q_len = len(self.queue)
        if q_len <= self.min_swaps:
            return int(q_len)
        length = q_len / 2
        if length > self.max_swaps:
            return int(self.max_swaps)
        return int(length)

    def fetch_swaps(self, pools):
        length = self.todo()
        self.sort(pools)
        result = self.queue[:length]
        self.queue = self.queue[length:]
        return result


class SwapQueueItem(Jsonable):
    def __init__(self, txn):
        self.txn = txn

        self.source_asset = txn.coins[0].asset
        parts = txn.memo.split(":")
        self.target_asset = Asset(parts[1])

    def calc_slip_and_fee(self, pools):
        # reset values
        self.fee = 0
        self.slip = 0
        self.score = 0

        inbound = self.txn.coins[0]
        for asset in [self.source_asset, self.target_asset]:
            if asset.is_rune():
                continue

            for pool in pools:
                if pool.asset != asset:
                    continue
                x = inbound.amount
                if inbound.asset.is_rune():
                    X = pool.rune_balance
                    Y = pool.asset_balance
                    out_asset = pool.asset
                else:
                    X = pool.asset_balance
                    Y = pool.rune_balance
                    out_asset = RUNE
                fee = self._calc_liquidity_fee(X, x, Y)
                if inbound.asset.is_rune():
                    fee = pool.get_asset_in_rune(fee)
                self.fee += fee
                self.slip += self._calc_swap_slip(X, x)

                inbound = Coin(out_asset, self._calc_asset_emission(X, x, Y))

    def _calc_liquidity_fee(self, X, x, Y):
        """
        Calculate the liquidity fee from a trade
        ( x^2 *  Y ) / ( x + X )^2

        :param int X: first balance
        :param int x: asset amount
        :param int Y: second balance
        :returns: (int) liquidity fee

        """
        return int(float((x ** 2) * Y) / float((x + X) ** 2))

    def _calc_swap_slip(self, X, x):
        """
        Calculate the trade slip from a trade
        expressed in basis points (10,000)
        x / (X + x)

        :param int X: first balance
        :param int x: asset amount
        :returns: (int) trade slip

        """
        swap_slip = 10000 * x / (X + x)
        return int(round(swap_slip))

    def _calc_asset_emission(self, X, x, Y):
        """
        Calculates the amount of coins to be emitted in a swap
        ( x * X * Y ) / ( x + X )^2

        :param int X: first balance
        :param int x: asset amount
        :param int Y: second balance
        :returns: (int) asset emission

        """
        return int((x * X * Y) / (x + X) ** 2)
