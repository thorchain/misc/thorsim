.PHONY: build lint format test test-watch shell

LOGLEVEL?=INFO
IMAGE_NAME=thorsim

clean:
	rm *.pyc

build:
	@docker build -t ${IMAGE_NAME} .

lint:
	@docker run --rm -v ${PWD}:/app pipelinecomponents/flake8:latest flake8

format:
	@docker run --rm -v ${PWD}:/app cytopia/black /app

test:
	@docker run ${IMAGE_NAME} python -m unittest tests/test_*

test-coverage:
	@docker run ${IMAGE_NAME} coverage run -m unittest tests/test_*

test-coverage-report:
	@docker run ${IMAGE_NAME} coverage report -m

test-watch:
	@PYTHONPATH=${PWD} ptw .

shell:
	@docker run ${DOCKER_OPTS} -it ${IMAGE_NAME} sh

# ----------- imported ----------- #

start-internal:
	python3 1_hyperinflation.py

# this leave postgres running
start:
	@docker run --rm ${IMAGE_NAME} make start-internal

sh:
	@docker run --rm --no-deps ${IMAGE_NAME} /bin/sh

run: build start

run-watch:
	find . -name '*.py' | entr -adr make start-internal
