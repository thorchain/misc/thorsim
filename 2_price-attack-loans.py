#!/usr/bin/env python
"""
simulating an attempt manipulate pool price when taking a loan so that the
attacker have a debt less than the real world value of the collaters
"""

import logging
import time
import os
from copy import copy

from utils.common import (
    Transaction,
    Coin,
    Asset,
    print_amount,
    get_rune_asset,
)

from thornode.thornode import THORNode
from thornode.pool import Pool
from chains import CHAINS

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)

RUNE = get_rune_asset()

def main():
    logging.info("*" * 50)
    results = []
    for x in range(1,50):
        thor = THORNode()

        runeUSD = 10.0
        assetUSD = 0.1
        asset = Asset("DOGE.DOGE")
        txid = "blahblah"
        pool = Pool(asset)
        thor.set_pool(pool)

        loaner = "LP_1"
        pool.add_liquidity("setup", "setup", 1*1e8, 100*1e8, txid)
        pool.add_liquidity(None, "LP_1", 1, 100*1e8, txid)
        thor.set_pool(pool)
        # print(thor.get_pool(asset).pprint())

        swap_txn = thor.generate_arb_txn(asset, 0.1)

        thor.add_swap_queue(swap_txn)
        thor.begin_block()
        thor.end_block()

        # print(thor.get_pool(asset).pprint())
        # x *= 10
        pool = thor.get_pool(asset)
        # pool.add_liquidity("LP_2", None, x*1e8, 0, txid)
        # lp_2 = pool.get_liquidity_provider(loaner)
        # print(thor.get_pool(asset).pprint())
        # lp_2_value = (lp_2.rune_deposit_value * runeUSD / 1e8) + (lp_2.asset_deposit_value * assetUSD / 1e8)
        # lp_2_loss = (x * runeUSD) - lp_2_value
        lp_2_loss = 0

        lp = pool.get_liquidity_provider(loaner)
        pool.open_loan(thor.height, loaner, lp.units)
        thor.set_pool(pool)

        loan = pool.get_loan(loaner)

        real_value = (loan.rune_deposit_value * runeUSD / 1e8) + (loan.asset_deposit_value * assetUSD / 1e8)

        # print(f"Attacker LP Loss: {lp_2_loss:,.2f}")
        print(f"Real Collateral Value: {real_value:,.2f}")
        print(f"Attacker Debt: {loan.debt:,.2f}")

        if real_value <= loan.debt - lp_2_loss:
            print(f">>>> Exploited!!!!💥: {loan.debt-real_value:,.2f}")
        else:
            print(">>>> OK")

        results.append(loan.debt - real_value - lp_2_loss)
    
    for i, r in enumerate(results):
        print(f"{i}\t{r:,.2f}")



if __name__ == "__main__":
    main()
