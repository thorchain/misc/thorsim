#!/usr/bin/env python
import logging
import time
import os
from tqdm import tqdm
from copy import copy

from utils.common import (
    Transaction,
    Coin,
    Asset,
    print_amount,
)

from thornode.thornode import THORNode
from chains import CHAINS
from chains.bitcoin import Bitcoin

# Init logging
logging.basicConfig(
    format="%(asctime)s | %(levelname).4s | %(message)s",
    level=os.environ.get("LOGLEVEL", "INFO"),
)


def runner(hodlers=100, size=10, rune_price_change=0.1, swaps_per_block=1):
    logging.info("*" * 50)
    thor = THORNode()
    thor.populate()
    original_height = thor.height
    original_rune = thor.supply['rune']

    logging.info(f"Hodlers Per asset: {hodlers}")
    logging.info(f"Size: {size}")
    logging.info(f"Rune Price Change: {rune_price_change}")
    logging.info(f"Swaps per block: {swaps_per_block}")

    # burn rune made for previous derived assets
    rune_burned = 0
    for c in [Bitcoin]:
        if c.chain == "THOR":
            c.coin = Asset("THOR.USD")
        asset = c.coin.get_derived_asset()
        pool = thor.get_pool(asset)
        thor.supply['rune'] -= pool.rune_balance * size
        rune_burned += pool.rune_balance * size
        thor.supply[str(asset)] = pool.asset_balance * size
    for pool in thor.pools:
        pool.rune_balance /= rune_price_change # change rune price
        if not pool.asset.is_derived_asset():
            continue
        supply = thor.get_supply(str(pool.asset))
        if supply == 0:
            continue

        logging.info(f"Initing swaps ({pool.asset})...")
        for x in tqdm(range(0, hodlers)):
            target_asset = copy(pool.asset)
            target_asset.chain = pool.asset.symbol
            if target_asset == "LUNA.LUNA":
                target_asset = Asset("TERRA.LUNA")
            if pool.asset == Asset("THOR.USD"):
                anchors = thor.get_usd_anchors()
                target_asset = anchors[x%len(anchors)]
            swap = Transaction(
                pool.asset.chain,
                f"{pool.asset.chain.lower()}1xxxxx",
                f"{target_asset.chain.lower()}1yyyyy",
                Coin(pool.asset, supply / hodlers),
                f"~:{target_asset}:{target_asset.chain.lower()}1yyyyy{x}",
            )
            thor.add_swap_queue(swap)
            if swaps_per_block > 0 and x % swaps_per_block == 0:
                thor.begin_block()
                thor.end_block()

    logging.info("finishing swaps...")
    length = thor.swap_queue.len()
    pbar = tqdm(total=thor.swap_queue.len())
    while thor.swap_queue.len() > 0:
        thor.begin_block()
        thor.end_block()
        pbar.update(length-thor.swap_queue.len())
        length=thor.swap_queue.len()
    pbar.close()

    diff_height = thor.height-original_height
    mins = diff_height * 5.6 / 60
    rune_inflation = thor.supply['rune']-original_rune
    print(f"Blocks processed: {diff_height} ({mins:,.2f} min)")
    print(f"Rune Supply Delta: {print_amount(thor.supply['rune']-original_rune)}")
    print(f"USD Yield: {print_amount(thor.usd_vault)}")
    for k,v in thor.liquidity.items():
        p = thor.get_pool(k)
        y = v/p.rune_balance*100
        print(f"{p.asset.chain}-{p.asset.symbol.split('-')[0]} Yield: {y:,.2f}% ({y * (525600.0/mins):,.2f}% APY)")

    logging.info("*" * 50)
    return rune_inflation

def main():
    results = []
    a, b, c, d = 200000, 360, 1, 100
    for x in range(1,11):
        val = x/10
        inflation = runner(a, b, val, d)
        results.append([val, inflation])

    for r in results:
        print(f"{r[0]}\t{print_amount(r[1])}")

if __name__ == "__main__":
    main()
